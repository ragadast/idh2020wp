<?php

/*cambios pagina login*/
//Cambiamos el logo
add_action('login_enqueue_scripts', 'bs_change_login_logo');
function bs_change_login_logo()
{ ?>

    <style type="text/css">
        #login h1 a {
            width: 300px;
            height: 200px;
            background-image: url(https://cursos.idhyoga.com/wp-content/uploads/2020/04/logo-idh.png);
            background-size: 300px 200px;
        }
    </style>

<?php }

//Cambiamos la URL del logo			  
add_filter('login_headerurl', 'bs_login_logo_url');
function bs_login_logo_url($url)
{
    return 'https://cursos.idhyoga.com/';
}

//Cambiamos el título de la URL del logo
add_filter( 'login_headertitle', 'bs_login_logo_url_title' );
function bs_login_logo_url_title() {
    return 'Cursos IDH Yoga';}