<?php get_header(); ?>
<div class="container-fluid">
    <div class="row mt-5">
    <div class="col-md-12 img-fluid mt-5 MB-5">
            <div class="img-fluid p-3 " style="background-image: url(https://cursos.idhyoga.com/wp-content/uploads/2020/07/imgdef.jpg); color:white">
                <div class="border p-5" style="background-color: rgba(255, 0, 0, 0.2);;">
                    <h1>Bienvenidos a nuestro blog</h1>
                    <h4>Utilizamos este espacio para compartir temas y reflexiones que esperamos te gusten y te sirvan</h4>
                    <h4>Estariamos muy contentos y felices en resivir tus comentarios</h4>
                </div>
            </div>
        </div>
        </div>
    <div class="row mt-5">
        
        <?php if (have_posts()) : while (have_posts()) : ?>
                <div class="col-md-6">
                    <?php the_post(); ?>
                    <?php if (has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink(); ?>">
                            <h1><?php the_title(); ?></h1>
                        </a>
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php the_post_thumbnail_url('post_image'); ?>" class="img-fluid mb-5" alt="<?php the_title(); ?>">
                        </a>
                    <?php endif ?>


                    <?php the_excerpt(); ?>
                </div>
            <?php endwhile; ?>

        <?php else : endif; ?>
        <div class="pagination">
            <?php
            global $wp_query;

            $big = 999999999; // need an unlikely integer

            echo paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages
            ));
            ?>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>