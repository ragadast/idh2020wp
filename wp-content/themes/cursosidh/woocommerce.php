<?php get_header(); ?>
<div id="heronoticias" class="d-none d-sm-none d-md-block">
    <div class="interior-principal">
        <h1 class="titulo">ACTIVIDADES</h1>
    </div>
</div>

<div class="separarmovil d-block d-sm-block d-md-none"></div>

<div id="heronoticiasmovil" class="d-block d-sm-block d-md-none">
    <div class="interior-principalmovil">
        <h1 class="titulo">ACTIVIDADES</h1>
    </div>
</div>

<div class="content pt-4">
    <div class="container">
        <div class="separarchica d-block d-sm-block d-md-none"></div>
        <DIV class="row">
            <div class="col-md-12 mt-3 mb-3">
                <h1>Clases, Cursos y Talleres</h1>
            </div>
        </DIV>
        <div class="row">
            <div class="col-lg-12">
                <?php woocommerce_content(); ?>
            </div>
        </div>
    </div>
</div>
<div id="herobajohome">
    <div class="interior-principal-2">
        <h1 class="titulo">CONOCE NUESTRO CATÁLOGO</h1>
        <h1 class="titulo">DE CLASES, CURSOS Y TALLERES</h1>
        <h4>Soluciones para tu bienestar y tranquilidad interior. </h4>
        <a href="https://cursos.idhyoga.com/tienda/">
            <button type="button" class="btn btn-success btn-lg mt-3">Visita nuestra tienda</button>
        </a>
    </div>
</div>
</div>
<?php get_footer(); ?>