<!-- pagina de BLOG -->
<?php get_header(); ?>
<div class="content pt-4 mt-5">
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
            <?php if (has_post_thumbnail()) : ?>
                <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url('post_image'); ?>" class="img-fluid mb-5 " alt="<?php the_title(); ?>" style="width: 100%">
                </a>
            <?php endif ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-lg-3">
                <div class="sticky-top d-none d-sm-none d-md-block" style="top:50px;">
                    <?php dynamic_sidebar('blog-sidebar'); ?>
                </div>

            </div>
            <div class="col-lg-9">
                <h1><?php the_title(); ?></h1>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile;
                else : endif; ?>
            </div>
        </div>
        <div class="row">
				<div class="col-md-12">
					<?php comments_template( '/comments.php'); ?>
				</div>
			</div>
    </div>
</div>
</div>
<?php get_footer(); ?>