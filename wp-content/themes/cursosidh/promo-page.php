
<?php
/*
    Template Name: promo
*/
?>
<?php get_header(); ?>
<div id="heropromo" class="d-none d-sm-none d-md-block">
    <div class="interior-principal">
        <h1>ACTIVIDADES PARA TI</h1>
    </div>
</div>
<div class="separarmovil d-block d-sm-block d-md-none"></div>

<div id="heropromomovil" class="d-block d-sm-block d-md-none">
    <div class="interior-principalmovil">
        <h1>ACTIVIDADES PARA TI</h1>
    </div>
</div>
<div class="content pt-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php if (has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url('post_image'); ?>" class="img-fluid mb-5" alt="<?php the_title(); ?>">
                    </a>
                <?php endif ?>

                <h1><?php the_title(); ?></h1>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile;
                else : endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>