<?php
@ini_set('upload_max_size', '128M');
@ini_set('post_max_size', '128M');
@ini_set('max_execution_time', '300');

include('function_woocommerce.php');
include('function_login.php');

///////agrega estilos 
function load_stylesheets()
{
    wp_register_style('stylesheet', get_template_directory_uri() . '/style.css', '', 1, 'all');
    wp_enqueue_style('stylesheet');

    wp_register_style('custom', get_template_directory_uri() . '/app.css', '', 1, 'all');
    wp_enqueue_style('custom');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

///////agrega javascrip
function load_javascript()
{
    wp_register_script('custom', get_template_directory_uri() . '/app.js', 'jquery', 1, true);
    wp_enqueue_script('custom');
}
add_action('wp_enqueue_scripts', 'load_javascript');


///agregar soportes
add_theme_support('menus');
add_theme_support('post-thumbnails');


///////agrega menu
if (!function_exists('mytheme_register_nav_menu')) {

    function mytheme_register_nav_menu()
    {
        register_nav_menus(array(
            'primary_menu' => __('Primary Menu', 'text_domain'),
            'footer_menu'  => __('Footer Menu', 'text_domain'),
            'primary' => __('Nav Menu', 'THEMENAME'),
            'carro' => __('Carro Menu', 'THEMENAME'),
        ));
    }
    add_action('after_setup_theme', 'mytheme_register_nav_menu', 0);
}

////tamaños de imagenes
add_image_size('post_image', 1100, 750, true);

////agregar sidebar widgets
function wpdocs_theme_slug_widgets_init()
{
    register_sidebar(array(
        'name'          => __('Principal Sidebar', 'lateral'),
        'id'            => 'sidebar-1',
        'description'   => __('Widgets in this area will be shown on all posts and pages.', 'textdomain'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'wpdocs_theme_slug_widgets_init');

function wpdocs_theme_slug_widgets_blog()
{
    register_sidebar(array(
        'name'          => __('blog Sidebar', 'blog'),
        'id'            => 'sidebar-2',
        'description'   => __('Widgets in this area will be shown on all posts and pages.', 'textdomain'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'wpdocs_theme_slug_widgets_blog');


function wpdocs_theme_slug_widgets_f3()
{
    register_sidebar(array(
        'name'          => __('f3 Sidebar', 'footer'),
        'id'            => 'sidebar-3',
        'description'   => __('Widgets in this area will be shown on all posts and pages.', 'textdomain'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'wpdocs_theme_slug_widgets_f3');

function wpdocs_theme_slug_widgets_producto()
{
    register_sidebar(array(
        'name'          => __('producto Sidebar', 'producto'),
        'id'            => 'productos',
        'description'   => __('Widgets in this area will be shown on all posts and pages.', 'textdomain'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'wpdocs_theme_slug_widgets_producto');





/**
 * Register Custom Navigation Walker
 */
function register_navwalker()
{
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');


/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more($more)
{
    if (!is_single()) {
        $more = sprintf(
            '<a href="%1$s">
        <svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="green" xmlns="https://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>
  <path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
</svg>%2$s</a>',
            get_permalink(get_the_ID()),
            __(' leer más', 'textdomain')
        );
    }

    return $more;
}
add_filter('excerpt_more', 'wpdocs_excerpt_more');


function idh_modify_comment_field($fields){

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields =  array(

	  'author' =>
	    '<p class="comment-form-author"><label for="author">' . __( 'Nombre' ,'twentysixteen' ) .
	    ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
	    '<input id="author" class="form-control" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
	    '" size="30"' . $aria_req . ' /></p>', 

	  'email' =>
	    '<p class="comment-form-email"><label for="email">' . __( 'Email' , 'twentysixteen' ) .
	    ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
	    '<input id="email" class="form-control"  name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	    '" size="30"' . $aria_req . ' /></p>',

	  'url' =>
	    '<p class="comment-form-url"><label for="url">' . __( 'Website' , 'twentysixteen' ) . '</label>' .
	    '<input id="url"  class="form-control"  name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
	    '" size="30" /></p>',
	);

	return $fields;
}

add_filter( 'comment_form_default_fields','idh_modify_comment_field' );

