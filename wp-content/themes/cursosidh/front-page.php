<?php get_header(); ?>



<div id="herohome" class="d-none d-sm-none d-md-block">

    <div class="interior-principal">

        <h1 class="titulo">BIENVENIDO AL MUNDO DEL</h1>

        <h1 class="titulo">YOGA Y EL DESARROLLO HUMANO</h1>

    </div>

</div>



<div class="separarmovil d-block d-sm-block d-md-none"></div>



<div id="herohomemovil" class="d-block d-sm-block d-md-none">

    <div class="interior-principalmovil">

        <h1 class="titulo">BIENVENIDO AL MUNDO DEL YOGA Y EL DESARROLLO HUMANO</h1>

    </div>

</div>



<div class="content">

    <div class="container">

        <div class="row d-none d-sm-none d-md-block">

            <div class="col-md-12 mt-5 mb-5 text-center">

                <H1>Instituto de Desarrollo Humano ONLINE</H1>

            </div>

        </div>

        <div class="row mb-4 d-block d-sm-block d-md-none">

            <div class="col-md-12 text-center mt-4">

                <H1>Instituto de Desarrollo Humano ONLINE</H1>

            </div>

        </div>

        <div class="row">

            <div class="col-md-6 ">

                <div class="cuadro1">

                    <div class="interior1 d-none d-sm-none d-md-block">





                        <p>Flexibilidad, fortaleza, resistencia, agilidad.</p>

                    </div>

                </div>

            </div>

            <div class="col-md-6 mt-5">

                <a href="http://idhecuador.org/tienda/">

                    <h2>Clases <strong>Online</strong> de Yoga, Pilates y Tai Chi</h2>

                </a>



                <P>

                    Clases que se desarrollan con ejercicios aptos para iniciar y conocer los fundamentos de la practica.



                    Basada en la integración cuerpo, mente y espíritu, la práctica de la Yoga conduce al equilibrio y

                    autodominio emocional, físico y mental, influyendo activamente en la personalidad. Así mismo,

                    todas las disciplinas (Pilates, Tai Chi, Yoga), nos dan la posibilidad de mejorar nuestra salud,

                    alcanzar un estado de paz y autodominio personal. Como sistema de vida nos plantea las bases para

                    lograr una realización interior.

                </P>

            </div>

        </div>

        <div class="separar d-none d-sm-none d-md-block"></div>

        <div class="row mt-3">

            <div class="col-md-6 d-block d-sm-block d-md-none">

                <div class="cuadro2 ">

                    <div class="interior2 d-none d-sm-none d-md-block">



                    </div>

                </div>

            </div>

            <div class="col-md-6 mt-5">

                <a href="http://idhecuador.org/tienda/">

                    <h2>Cursos <strong>Online</strong> de Meditación y Desarrollo Humano</h2>

                </a>

                <p>La práctica constante de la meditación ayuda al autodominio psicológico, la limpieza y serenidad de la mente, la unificación de los rayos mentales habitualmente dispersos y la revelación de la verdadera naturaleza interior de todas las cosas »</p>

                <p><strong>La meditación es una disciplina practicada desde la antigüedad con un propósito fundamentalmente espiritual.</strong> </p>

                <P>

                    Es una práctica consistente en la concentración sostenida de la atención en una única dirección o en un único objeto, con el propósito de adquirir un dominio de la mente y una liberación de sus trampas y cadenas. El ejercicio meditativo facilita el aquietamiento de la mente y, en consecuencia, la conquista de un estado de serenidad favorable para lograr un conocimiento profundo de sí mismo y una transformación personal.

                </P>

            </div>

            <div class="col-md-6">

                <div class="cuadro2 d-none d-sm-none d-md-block">

                    <div class="interior1 d-none d-sm-none d-md-block">



                        <p>Herramientas para desarrollar la paz interior, tranquilidad, concentración.</p>

                    </div>

                </div>

            </div>

        </div>

        <div class="separar1 d-none d-sm-none d-md-block"></div>

        <div class="row">

            <div class="col-md-6 mt-3">

                <div class="cuadro3">

                    <div class="interior1 d-none d-sm-none d-md-block">

                        <p>Intereses variados y holísticos.</p>

                    </div>

                </div>

            </div>

            <div class="col-md-6 mt-5">

                <a href="http://idhecuador.org/tienda/">

                    <h2>Talleres <strong>Online</strong> de Desarrollo Personal, Educativos y de Intereses Variados.</h2>

                </a>

                <P>

                    Diferentes cursos y talleres pensados para tu desarrollo interior, abarcando temáticas holísticas, de desarrollo humano, de psicología, y variados temas de interés general. Dirigido para todos aquellos que buscan una mejor calidad de vida.

                </P>

            </div>

        </div>

        <div class="separar d-none d-sm-none d-md-block"></div>

    </div>

    <div id="herobajohome">

        <div class="interior-principal-2">

            <h1 class="titulo">CONOCE NUESTRO CATÁLOGO</h1>

            <h1 class="titulo">DE CLASES, CURSOS Y TALLERES</h1>

            <h4>Soluciones para tu bienestar y tranquilidad interior. </h4>

            <a href="http://idhecuador.org/tienda/">

                <button type="button" class="btn btn-success btn-lg mt-3">Ver catálogo</button>

            </a>

        </div>

    </div>

</div>





<?php get_footer(); ?>