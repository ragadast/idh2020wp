<?php get_header(); ?>
<div id="herohome" class="d-none d-sm-none d-md-block">
    <div class="interior-principal">
        <h1 class="titulo">BIENVENIDO AL MUNDO DEL</h1>
        <h1 class="titulo">YOGA Y EL DESARROLLO HUMANO</h1>
    </div>
</div>
<div class="content pt-4">
    <div class="container">
    <div class="separar d-block d-sm-block d-md-none"></div>
        <div class="row">
            <div class="col-lg-12">
                <?php if (has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url('post_image'); ?>" class="img-fluid mb-5" alt="<?php the_title(); ?>">
                    </a>
                <?php endif ?>

                <h1><?php the_title(); ?></h1>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile;
                else : endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>