<?php 
/*
    Template Name: tienda
*/
?>
<?php get_header(); ?>
<div id="herobajo" class="d-none d-sm-none d-md-block">
    <div class="interior-principal">
        <h1 class="titulo">BIENVENIDO<br>
            AL MUNDO ECO</h1>
        <h4>Creémos juntos una economía ecológica de mercado</h4>
    </div>
</div>
<div class="content pt-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>paginaaaaaaaaaaaaaa tienda</h1>
                <?php if (has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url('post_image'); ?>" class="img-fluid mb-5" alt="<?php the_title(); ?>">
                    </a>
                <?php endif ?>

                <h1><?php the_title(); ?></h1>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile;
                else : endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>