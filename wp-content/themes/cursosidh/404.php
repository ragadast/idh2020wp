<?php get_header(); ?>

<div id="herohome" class="d-none d-sm-none d-md-block">
    <div class="interior-principal">
        <h1 class="titulo">BIENVENIDO AL MUNDO DEL</h1>
        <h1 class="titulo">YOGA Y EL DESARROLLO HUMANO</h1>
    </div>
</div>

<div class="separarmovil d-block d-sm-block d-md-none"></div>

<div id="herohomemovil" class="d-block d-sm-block d-md-none">
    <div class="interior-principalmovil">
        <h1 class="titulo">BIENVENIDO AL MUNDO DEL YOGA Y EL DESARROLLO HUMANO</h1>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5 mb-5 text-center">
                <H1>Página no encontrada</H1>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>